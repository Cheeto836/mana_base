#include <iostream>

#include "deck_parser.hpp"

int main(int argc, char** argv)
{
  auto deck = DeckParser::parseDeck(argv[1]);

  std::cout << deck.name << " mana breakdown" << std::endl;

  CastingCost pipBreakdown {0, 0, 0, 0, 0, 0};
  for (const auto& card : deck.cards)
  {
    std::cout << "\t" << card.name << ": w(" << card.cost.white << ") u(" << card.cost.blue << ") b(" << card.cost.black << ") r(" << card.cost.red << ") g(" << card.cost.green << ")" << std::endl;
    pipBreakdown.white += card.cost.white;
    pipBreakdown.blue += card.cost.blue;
    pipBreakdown.black += card.cost.black;
    pipBreakdown.red += card.cost.red;
    pipBreakdown.green += card.cost.green;
  }

  const auto totalPips = pipBreakdown.white + pipBreakdown.blue + pipBreakdown.black + pipBreakdown.red + pipBreakdown.green;
  std::cout << "white pips: " << pipBreakdown.white << " (" << ((double)pipBreakdown.white / (double)totalPips) * 100.0 << "%)" << std::endl;
  std::cout << "blue pips: " << pipBreakdown.blue << " (" << ((double)pipBreakdown.blue / (double)totalPips) * 100.0 << "%)" << std::endl;
  std::cout << "black pips: " << pipBreakdown.black << " (" << ((double)pipBreakdown.black / (double)totalPips) * 100.0 << "%)" << std::endl;
  std::cout << "red pips: " << pipBreakdown.red << " (" << ((double)pipBreakdown.red / (double)totalPips) * 100.0 << "%)" << std::endl;
  std::cout << "green pips: " << pipBreakdown.green << " (" << ((double)pipBreakdown.green / (double)totalPips) * 100.0 << "%)" << std::endl;
  return 0;
}
