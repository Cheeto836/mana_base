#include "deck_parser.hpp"

#include <fstream>
#include <iostream>
#include <regex>

#include "json.hpp"

int countMana(const std::string& costStr, const std::string& manaStr)
{
  const std::regex costRegex(manaStr);

  return std::distance(std::sregex_iterator(costStr.begin(), costStr.end(), costRegex), std::sregex_iterator());
}

CastingCost parseCost(const nlohmann::json& costJson)
{
  const auto costStr = costJson.get<std::string>();

  return {0,
          countMana(costStr, "W"),
          countMana(costStr, "U"),
          countMana(costStr, "B"),
          countMana(costStr, "R"),
          countMana(costStr, "G")};
}

Card parseCard(const nlohmann::json& cardJson)
{
  const auto cardName = cardJson["name"];
  const auto cost = parseCost(cardJson["mana_cost"]);

  return {cardName, cost};
  return Card();
}

Deck DeckParser::parseDeck(const std::string& filePath)
{
  std::ifstream f(filePath);
  if (!f.is_open())
  {
    std::cerr << "Failed to open file '" << filePath << "' for reading" << std::endl;
    return Deck();
  }

  try
  {
    nlohmann::json j;
    f >> j;
    Deck d;

    d.name = j["name"];

    //handle commander(s)
    for (const auto& obj : j["entries"]["commanders"])
    {
      const auto c = obj["card_digest"];
      if (!c.is_null())
      {
        d.cards.push_back(parseCard(c));
      }
    }

    //handle non-lands
    for (const auto& obj : j["entries"]["nonlands"])
    {
      const auto c = obj["card_digest"];
      if (!c.is_null())
      {
        d.cards.push_back(parseCard(c));
      }
    }

    return d;
  }
  catch(const std::exception& e)
  {
    std::cerr << "Failed to parse deck JSON '" << filePath << "'\nerror: " << e.what() << std::endl;
    return Deck();
  }
}

