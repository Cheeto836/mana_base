#pragma once

#include <string>
#include <vector>

struct CastingCost
{
  int generic;
  int white;
  int blue;
  int black;
  int red;
  int green;
};

struct Card
{
  std::string name;
  CastingCost cost;
};

struct Deck
{
  std::string name;
  std::vector<Card> cards;
};

class DeckParser
{
  private:
    DeckParser() = default;
  public:
    static Deck parseDeck(const std::string& filePath);
};
